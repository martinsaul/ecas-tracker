package io.saul.citizenshiptracker.service;

import io.saul.citizenshiptracker.ZipUtil;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermissions;

@Service
public class ChromeDriverRetrievalService {

    private static final File chromeFolder = new File(new File("chrome").getAbsolutePath());
    private final static Logger log = LoggerFactory.getLogger(ChromeDriverRetrievalService.class);

    private final File chromeExec;
    private final File chromeDriver;

    @Autowired
    public ChromeDriverRetrievalService() throws IOException, URISyntaxException {
        File sourceZip = null;
        if (SystemUtils.IS_OS_WINDOWS) {
            chromeExec = new File(chromeFolder, "chrome.exe");
            chromeDriver = new File(chromeFolder, "chromedriver.exe");
            sourceZip = new File("87_win.zip");
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            //TODO Add support to MAC by auto-detecting chrome and downloading the appropriate chrome driver.
            //Not a priority.
            throw new RuntimeException("MAC is unsupported at this time.");
        } else if (SystemUtils.IS_OS_LINUX) {
            chromeExec = new File(chromeFolder, "chrome");
            chromeDriver = new File(chromeFolder, "chromedriver");
            sourceZip = new File("87_lin.zip");
        } else {
            throw new RuntimeException("Unknown OS");
        }

        if(!chromeFolder.exists()) {
            chromeFolder.mkdirs();
            log.info("Chrome not present. Setting up...");
            log.info("Extracting Chome.");
            ZipUtil.unzipFolder(sourceZip, chromeFolder.toPath());
            log.info("Successfully extracted.");
            if (SystemUtils.IS_OS_LINUX) {
                log.info("Adding execute permissions to files.");
                Files.setPosixFilePermissions(chromeExec.toPath(), PosixFilePermissions.fromString("rw-r--r-x"));
                Files.setPosixFilePermissions(chromeDriver.toPath(), PosixFilePermissions.fromString("rw-r--r-x"));
                log.info("Permissions granted...");
            }
        }
    }

    public File getChromeDriver() {
        return chromeDriver;
    }

    public File getChromeExec() {
        return chromeExec;
    }
}
