package io.saul.citizenshiptracker.service;

import com.google.gson.Gson;
import io.saul.citizenshiptracker.data.PresentStatus;
import io.saul.citizenshiptracker.exception.ECASException;
import io.saul.citizenshiptracker.properties.StatusProperties;
import io.saul.citizenshiptracker.service.notification.GMailNotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Random;

@Service
public class SchedulerService {

    public static final File STATUS_PROP_FILE = new File("status.json");
    private final CitizenshipAppTrackerService citizenshipAppTrackerService;
    private final GMailNotificationService gMailNotificationService;
    private final static Logger log = LoggerFactory.getLogger(SchedulerService.class);
    private final String destination;
    private final Random random;

    @Autowired
    public SchedulerService(CitizenshipAppTrackerService citizenshipAppTrackerService, GMailNotificationService gMailNotificationService, @Value("${gmail.sender}") String destination) {
        this.citizenshipAppTrackerService = citizenshipAppTrackerService;
        this.gMailNotificationService = gMailNotificationService;
        this.destination = destination;
        random = new Random();
    }

    @PostConstruct
    private void fireOne(){
        new Thread(this::run).start();
    }

    @Scheduled(cron = "0 0 10,14,18 ? * MON,TUE,WED,THU,FRI")
    public void hourlyScan(){
        runWithDelay();
    }

    private void runWithDelay() {
        final int randomMinutes = Math.abs(random.nextInt())%15;
        final int randomOther = Math.abs(random.nextInt()% 60000);
        try {
            log.info("Waiting " + randomMinutes + " minutes and " + ((double)randomOther)/1000 + " seconds.");
            Thread.sleep(randomMinutes * 60 * 1000 + randomOther);
            run();
        } catch (InterruptedException ignore) {
            log.error("Wait for execution interrupted.");
        }
    }

    private void run() {
        try {
            log.info("Starting execution...");
            StatusProperties status = getProperty();
            if(status == null)
                status = new StatusProperties();
            PresentStatus resultingData = citizenshipAppTrackerService.fireECASUpdates();
            StringBuilder changes = new StringBuilder();
            compare(status, changes, resultingData.application);
            String strChanges = changes.toString();
            if(!strChanges.isEmpty()) {
                try {
                    log.info(strChanges);
                    gMailNotificationService.send(destination, "Application status update!", strChanges); // TOOD hide this behind a generic notification service that is extensible.
                    saveProperty(status);
                } catch (IOException | MessagingException e) {
                    log.error("Failed to send email due to exception.", e);
                }
            } else
                log.info("No updates.");
        } catch (ECASException e) {
            log.error("Failed to retrieve application status due to exception.",e);
        }
    }

    private void compare(StatusProperties status, StringBuilder changes, Map<String, String> candidate) {
        for (String name : candidate.keySet()) {
            String updated = candidate.get(name);
            if (status.applicantStatus.containsKey(name)) {
                String current = status.applicantStatus.get(name);
                if (!current.equalsIgnoreCase(updated)) {
                    changes.append(MessageFormat.format("{0} has changed from {1} to {2}\n", name, current, updated));
                    status.applicantStatus.put(name, updated);
                }
            } else {
                changes.append(MessageFormat.format("{0} is now listed as {1}\n", name, updated));
                status.applicantStatus.put(name, updated);
            }
        }
    }


    private StatusProperties getProperty() {
        try {
            if(!STATUS_PROP_FILE.exists() && STATUS_PROP_FILE.createNewFile())
                return null; //TODO Improve this
            Reader reader = Files.newBufferedReader(Path.of(STATUS_PROP_FILE.toURI()));
            return new Gson().fromJson(reader, StatusProperties.class);
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    private void saveProperty(StatusProperties object) {
        try {
            FileWriter writer = new FileWriter(STATUS_PROP_FILE);
            new Gson().toJson(object, writer);
            writer.flush();
            writer.close();
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

}
