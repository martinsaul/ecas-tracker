package io.saul.citizenshiptracker.service;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.saul.citizenshiptracker.data.PresentStatus;
import io.saul.citizenshiptracker.exception.ECASException;
import io.saul.citizenshiptracker.properties.FormsProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.FluentWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.google.common.collect.ImmutableList.toImmutableList;

@Service
public class CitizenshipAppTrackerService {

    private final boolean headless;

    public static final File FORMS_PROP_FILE = new File("forms.json");
    private final static Logger log = LoggerFactory.getLogger(CitizenshipAppTrackerService.class);
    private final File chromeBinary;

    @Autowired
    public CitizenshipAppTrackerService(@Value("${headless:true}")boolean headless, ChromeDriverRetrievalService chromeDriverService){
        this.headless = headless;
        chromeBinary = chromeDriverService.getChromeExec();
        System.setProperty("webdriver.chrome.driver", chromeDriverService.getChromeDriver().getAbsolutePath());
        System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
    }

    private WebDriver navigateAndLogin() {
        FormsProperties form = getProperty();
        WebDriver driver = getWebDriver();
        driver.get("https://cst-ssc.apps.cic.gc.ca/en/login");
        FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
                .withTimeout(Duration.of(600, ChronoUnit.SECONDS))
                .pollingEvery(Duration.of(200, ChronoUnit.MILLIS))
                .ignoring(org.openqa.selenium.NoSuchElementException.class);

        fluentWait.until(webDriver -> webDriver.findElement(By.name("uci")));
        driver.findElement(By.name("uci")).sendKeys(form.credentials.uci);
        driver.findElement(By.name("password")).sendKeys(form.credentials.password);
        driver.findElement(By.id("sign-in-submit-btn")).click();
        return driver;
    }

    public PresentStatus fireECASUpdates() throws ECASException {
        WebDriver driver = null;
        try {
            driver = navigateAndLogin();
            FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
                    .withTimeout(Duration.of(600, ChronoUnit.SECONDS))
                    .pollingEvery(Duration.of(200, ChronoUnit.MILLIS))
                    .ignoring(org.openqa.selenium.NoSuchElementException.class);
            String currentUrl = "";
            do {
                currentUrl = driver.getCurrentUrl();
            } while(currentUrl.equalsIgnoreCase("https://cst-ssc.apps.cic.gc.ca/en/login"));
            if(currentUrl.equalsIgnoreCase("https://cst-ssc.apps.cic.gc.ca/en/dashboard")){
                fluentWait.until(webDriver -> webDriver.findElement(By.className("mt-15")));
                final ImmutableList<String> title = driver.findElement(By.className("mt-15")).findElements(By.tagName("h3")).stream().map(WebElement::getText).filter(text -> !text.isBlank()).collect(toImmutableList());
                final ImmutableList<String> status = driver.findElement(By.className("mt-15")).findElements(By.tagName("p")).stream().map(WebElement::getText).filter(text -> !text.isBlank() && !text.contains("status")).collect(toImmutableList());

                return getPresentStatus(title, status);
            } else throw new ECASException("Redirected to unknown URL: " + currentUrl);
        } finally {
            if(driver != null)
                driver.quit();
        }
    }

    private PresentStatus getPresentStatus(ImmutableList<String> title, ImmutableList<String> status) {
        PresentStatus result = new PresentStatus();

        result.application = IntStream.range(0, Math.min(title.size(), status.size()))
                .boxed()
                .collect(Collectors.toMap(title::get, status::get));
        return result;
    }

    private WebDriver getWebDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setBinary(chromeBinary);
        if(headless)
            options.addArguments("--headless");
        return new ChromeDriver(options);
    }

    private FormsProperties getProperty() { // THIS WILL BE PORTED INTO A SERVICE LATER.
        try {
            if(!FORMS_PROP_FILE.exists()){
                setDefaultProperty();
                log.error("Form configuration missing. Make sure to update " + FORMS_PROP_FILE.getAbsolutePath() + " with your credentials before starting again.");
                System.exit(1);
            }
            Reader reader = Files.newBufferedReader(Path.of(FORMS_PROP_FILE.toURI()));
            return new Gson().fromJson(reader, FormsProperties.class);
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    private void setDefaultProperty() {
        try {
            FileWriter writer = new FileWriter(FORMS_PROP_FILE);
            new GsonBuilder().setPrettyPrinting().create().toJson(new FormsProperties(), writer);
            writer.flush();
            writer.close();
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

}
