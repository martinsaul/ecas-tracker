package io.saul.citizenshiptracker.exception;

public class ECASException extends Exception {
    public ECASException(String message) {
        super(message);
    }
}
