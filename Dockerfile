FROM alpine:latest

WORKDIR /usr/app/ecas/build
RUN mkdir -p /usr/app/ecas/bin
RUN apk add nano curl tzdata chromium
RUN apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
RUN cp /usr/share/zoneinfo/America/Toronto /etc/localtime
RUN echo "America/Toronto" >  /etc/timezone
COPY 87_lin.zip ../bin
COPY credentials.json ../bin
COPY application.properties ../bin
COPY forms.json ../bin
COPY gradle/ ./gradle/
COPY gradlew ./
RUN ./gradlew -v
COPY build.gradle ./
COPY src/ ./src/
RUN ls -l
RUN ./gradlew build
RUN cp build/libs/*.jar ../bin
WORKDIR /usr/app/ecas/bin
RUN rm -rf /usr/app/ecas/build
RUN mv *.jar target.jar

CMD ["java", "-jar", "target.jar"]